	</div><!-- #main -->
	<footer id="footer" class="col-xs-12">
	<?php
		if ( has_nav_menu( 'footer-menu' ) ) {
			wp_nav_menu( array( 'theme_location' => 'footer-menu' ) );
		}

		global $wpdb;
    	$table = $wpdb->prefix.'footer';
    	global $polylang;
    	$lang = $polylang->curlang->slug;
		$text_footer = $wpdb->get_results( "SELECT text_footer FROM ".$table." WHERE lang = '".$lang."'");
		$text_footer = $text_footer[0]->text_footer;
	?>
	<p><?php echo $text_footer; ?></p>
	</footer>
	<?php wp_footer(); ?>
	</div><!-- #page -->
</body>
</html>