<?php 

/*
Template Name: NOTICIAS
*/

get_header(); ?>
<div id="sidebar" class="col-sm-4 hidden-responsive">
    <?php get_sidebar(); ?>
</div>
<div id="content" class="col-sm-8 col-xs-12 clearfix">
	<h1><?php the_title();?></h1>
  <?php $args = array(
                            'posts_per_page'   => get_option('posts_per_page'),
                            'offset'           => (get_option('posts_per_page')*(!isset($paged)  ? 0 : ($paged == 0 ? 0 : $paged-1))),
                            'orderby'          => 'date',
                            'order'            => '',
                            'post_type'        => 'post',
                            'post_status'      => 'publish',
                            'suppress_filters' => true
                    );
                    $posts_array = get_posts( $args );
					
					

                    foreach ( $posts_array as $post ) : setup_postdata( $post ); 
					
					$adjunto = get_post_meta( $id,'wpcf-adjunto-noticia', true);
					?>

					
	
		<article>
			<?php if ($adjunto==""):?>
			<h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
			<?php else :?>
			<h3><a href="<?php echo $adjunto;?>"><?php the_title();?></a></h3>
			<?php endif;?>
			<?php echo the_content();?>
			
		</article>
	
					
					
					
					
    <?php  endforeach; wp_reset_postdata();?>
<div id="sidebar" class="show-responsive">
    <?php get_sidebar('responsive'); ?>
</div>
<?php get_footer(); ?>