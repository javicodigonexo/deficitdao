<?php get_header(); ?>
<div id="sidebar" class="col-sm-4 hidden-responsive clearfix">
    <?php get_sidebar(); ?>
</div>
<div id="content" class="col-sm-8 col-xs-12 ">
	<h1><?php echo get_the_title(); ?></h1>
    <?php
        // Start the Loop.
		while ( have_posts() ) : the_post();
			echo the_content();
		endwhile;
	?>
</div>
<div id="sidebar" class="show-responsive col-xs-12 clearfix">
   <?php get_sidebar('responsive'); ?>
</div>
<?php get_footer(); ?>