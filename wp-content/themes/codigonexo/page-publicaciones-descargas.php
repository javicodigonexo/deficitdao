<?php 

/*
Template Name: descargas
*/

get_header(); ?>
<div id="sidebar" class="col-sm-4 hidden-responsive">
    <?php get_sidebar(); ?>
</div>
<div id="content" class="col-sm-8 col-xs-12">
	<?php 
		$rol = 'public';
		if(count((wp_get_current_user()->roles)) > 0){
			$rol = wp_get_current_user()->roles[0];
		}
	?>
	<?php $args = array(
			//'posts_per_page'   => get_option('posts_per_page'),
			//'offset'           => (get_option('posts_per_page')*(!isset($paged)  ? 0 : ($paged == 0 ? 0 : $paged-1))),
			'orderby'          => 'date',
			'order'            => 'DES',
			'post_type'        => 'download',
			'post_status'      => 'publish',
			'meta_key'			=> 'wpcf-public',
			'meta_value'		=> 'public',
			'suppress_filters' => true
	);
	$public = get_posts( $args );

	if($rol != 'public'){
	$args = array(
			//'posts_per_page'   => get_option('posts_per_page'),
			//'offset'           => (get_option('posts_per_page')*(!isset($paged)  ? 0 : ($paged == 0 ? 0 : $paged-1))),
			'orderby'          => 'date',
			'order'            => 'DES',
			'post_type'        => 'download',
			'post_status'      => 'publish',
			'meta_key'			=> 'wpcf-'.$rol,
			'meta_value'		=> $rol,
			'suppress_filters' => true
	);
	$profile = get_posts( $args );
	}else{
		$profile = array();
	}

	$posts_array = array_merge($public, $profile);

	foreach ( $posts_array as $post ) : setup_postdata( $post ); 
		$vo = get_post_meta( $post->ID,'wpcf-subir-archivo-v-o', true);
		$vt = get_post_meta( $post->ID,'wpcf-subir-archivo-v-t', true);
	?>
	<div class="row clearfix descarga">
		<div class="col-sm-10 col-xs-12texto">
			<p class="titulo_descarga"><?php echo get_the_title(); ?></p>
			<?php the_content();?>
		</div>
		<div class="col-sm-2 col-xs-12">
			<a href="<?php echo $vo;?>" class="enlace_descarga" download="download">
				<div>Download V.O</div>
			</a>
			<a href="<?php echo $vt;?>" class="enlace_descarga" download="download">
				<div>Download V.T</div>
			</a>
		</div>
		
	</div>
	<hr>
    <?php endforeach; wp_reset_postdata();?>
<div id="sidebar" class="show-responsive">
    <?php get_sidebar('responsive'); ?>
</div>
<?php get_footer(); ?>