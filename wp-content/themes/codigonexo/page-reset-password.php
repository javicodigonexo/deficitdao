<?php 
/*
Template Name: reset_password
*/

get_header(); ?>
<div id="sidebar" class="hidden-responsive">
    <?php get_sidebar(); ?>
</div>
<div id="content">
<?php if (is_user_logged_in()) { ?>
	
    <?php
    if (isset($_POST['reset_pass']))
    {
        $password = trim($_POST['user_pass']);
        $password2 = trim($_POST['user_pass2']);
        $errors = array();

        if($password != $password2){
            $errors[] = '<p>' . pll_e('Las contraseñas no coinciden') . '</p>';
        }else{
            wp_set_password( $password, get_current_user_id() );
            $errors[] = '<p>' . pll_e('Contraseña guardada correctamente') . '</p>';
        }
    }
    ?>
    <?php if (isset($_POST['reset_pass'])){ ?>
        <div>
            <?php 
                foreach ($errors as $key => $value) { 
                    echo $value;
                } 
            ?>
        </div>
    <?php } ?>
    
    <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>" class="">
        <div class="password">
            <label for="user_pass" ><?php pll_e('Nueva contraseña'); ?>: </label>
            <input type="text" name="user_pass" value="" size="20" id="user_pass" tabindex="1001" />
        </div>
        <div class="password-confirm">
            <label for="user_pass2" ><?php pll_e('Repetir nueva contraseña'); ?>: </label>
            <input type="text" name="user_pass2" value="" size="20" id="user_pass2" tabindex="1001" />
        </div>
        <div class="login_fields">
            <?php do_action('login_form', 'resetpass'); ?>
            <input type="submit" id="wp-submit" name="submit-pass" value="<?php pll_e('Cambiar'); ?>" class="" tabindex="1002" />
            <input type="hidden" name="reset_pass" value="1" />
            <input type="hidden" name="user-cookie" value="1" />
        </div>
    </form>
<?php }else{ ?>
    <div><?php pll_e('Permiso denegado'); ?></div>
<?php } ?>
<?php wp_reset_postdata();?>
<div id="sidebar" class="show-responsive">
    <?php get_sidebar('responsive'); ?>
</div>
<?php get_footer() ?>