<?php
/*
Template Name: login
*/

get_header(); ?>
<div id="sidebar" class="hidden-responsive">
    <?php get_sidebar(); ?>
</div>
<div id="content">
	<?php
	  $currentlang = get_bloginfo('language');
	  if ($currentlang == "es-ES"): ?>
    <h2><?php pll_e( 'Área de Socios' ); ?></h2>
	<?php elseif ($currentlang == "en-GB"): ?>
	<h2><?php pll_e( 'Member Zone' ); ?></h2>
	<?php endif;?>
	<?php while ( have_posts() ) : the_post();
			echo the_content();
		endwhile; ?>
    
	
	
	<?php if(isset($_GET['login']) && $_GET['login'] == 'failed'){ ?>
        <div class="alert alert-danger"><?php pll_e('Usuario o contraseña incorrectos'); ?></div>
    <?php    }

        if (is_user_logged_in()) {
           // echo '<div class="aa_logout"> Hello, <div class="aa_logout_user">', $user_login, '. You are already logged in.</div><a id="wp-submit" href="', wp_logout_url(), '" title="Logout">Logout</a></div>';
        } else {
             wp_login_form($args);
                $args = array(
                    'echo'           => true,
                    'redirect'       => home_url('/'), 
                    'form_id'        => 'loginform',
                    'label_username' => __( 'Username' ),
                    'label_password' => __( 'Password' ),
                    'label_remember' => __( 'Remember Me' ),
                    'label_log_in'   => __( 'Log In' ),
                    'id_username'    => 'user_login',
                    'id_password'    => 'user_pass',
                    'id_remember'    => 'rememberme',
                    'id_submit'      => 'wp-submit',
                    'remember'       => true,
                    'value_username' => NULL,
                    'value_remember' => true
                ); 
        }
    ?> 
	

<?php wp_reset_postdata();?>
<div id="sidebar" class="show-responsive">
    <?php get_sidebar('responsive'); ?>
</div>
<?php get_footer(); ?>