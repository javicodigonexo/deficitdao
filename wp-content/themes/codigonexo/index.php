<?php get_header(); ?>
<div id="main-content" class="main-content">
	<?php
		// Start the Loop.
		while ( have_posts() ) : the_post();
			echo the_content();
		endwhile;
	?>
</div>
<?php get_footer(); ?>