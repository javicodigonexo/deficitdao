<?php
	include "lessCompiler/lessc.inc.php";
	$less = new lessc;
	try {
		$less->checkedCompile(get_template_directory()."/style.less", get_template_directory()."/style.css");
	} catch (Exception $ex) {
		echo "lessphp fatal error: ".get_template_directory()."/".$ex->getMessage();
	}
?>
<!DOCTYPE html>
<html lang="es-ES">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link href="<?php echo get_template_directory_uri(); ?>/style.css" rel="stylesheet" type="text/css">
	<?php // Marcos Navarro - Codigonexo - Adelanto la carga de bootstrap porque sí no pisa a los estilos que importado de la web antigua ?>
	<?php wp_head(); ?>
	<link href="http://fonts.googleapis.com/css?family=Oswald&amp;v1" rel="stylesheet" type="text/css">
	<link href="<?php echo get_template_directory_uri(); ?>/css/reset.css" rel="stylesheet" type="text/css">
	<link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet" type="text/css">
	<link href="<?php echo get_template_directory_uri(); ?>/css/css_custom.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="wrapper" class="row clearfix">
	<header id="header">
		<div class="row cleafix">
			<div class="col-sm-5 bg-izquierda">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><div class="logo"></div></a>
			</div>
			<div class="col-sm-7 bg-derecha">
				<div class="lang">
					<?php
						if ( has_nav_menu( 'header-idiomas' ) ) {
							echo wp_nav_menu( array( 'theme_location' => 'header-idiomas' ) );
						}
					?>
					<?php if (is_user_logged_in()) { ?>
						 <!--cadena de texto otro idioma-->
							<?php
							  $currentlang = get_bloginfo('language');
							  if ($currentlang == "es-ES"): ?>
							<a href="<?php echo esc_url( home_url( '/' ).'?p=172' ); ?>" class="area-usuarios"><?php pll_e('Panel'); ?></a> | <a href="<?php echo wp_logout_url( get_permalink() ); ?>" class="salir"><?php pll_e('Salir'); ?></a></a>
							<?php elseif ($currentlang == "en-GB"): ?>
							<a href="<?php echo esc_url( home_url( '/' ).'?p=172' ); ?>" class="area-usuarios"><?php pll_e('Panel'); ?></a> | <a href="<?php echo wp_logout_url( get_permalink() ); ?>" class="salir"><?php pll_e('Logout'); ?></a></a>

							<?php endif;?>
						<!-- FIN cadena de texto otro idioma-->

					<?php } else { ?>

						 <!--cadena de texto otro idioma-->
							<?php
							  $currentlang = get_bloginfo('language');
							  if ($currentlang == "es-ES"): ?>
							<a href="<?php echo esc_url( home_url( '/' ).'?p=178' ); ?>" class="area-usuarios"><?php pll_e('Área de socios'); ?></a>
							<?php elseif ($currentlang == "en-GB"): ?>
							<a href="<?php echo esc_url( home_url( '/' ).'?p=178' ); ?>" class="area-usuarios"><?php pll_e('Member Zone'); ?></a>

							<?php endif;?>
						<!-- FIN cadena de texto otro idioma-->
					<?php } ?>
				</div>
			</div>
		</div>
		<div class="show-responsive menu-nav">
			<div class="menu-responsive"></div>
		</div>
		<div class="row clearfix menuNav hidden-responsive ">
			<?php
			echo wp_nav_menu(array('theme_location'  => 'header-menu',
											'menu'            => '',
											'container'       => 'div',
											'container_class' => '',
											'container_id'    => 'nav',
											'menu_class'      => 'menu',
											'menu_id'         => '',
											'echo'            => true,
											'fallback_cb'     => 'wp_page_menu',
											'before'          => '',
											'after'           => '',
											'link_before'     => '',
											'link_after'      => '',
											'items_wrap'      => '<ul>%3$s</ul>',
											'depth'           => 2,
											'walker'          => ''));
			?>
		</div>
		<div class="row clearfix menuNav show-responsive responsive">

                    <?php
                      $currentlang = get_bloginfo('language');
                      if ($currentlang == "es-ES"): ?>




			<?php
			echo wp_nav_menu(array('theme_location'  => 'header-menu',
											'menu'            => 'menu-responsive',
											'container'       => 'div',
											'container_class' => '',
											'container_id'    => 'nav',
											'menu_class'      => 'menu',
											'menu_id'         => '',
											'echo'            => true,
											'fallback_cb'     => 'wp_page_menu',
											'before'          => '',
											'after'           => '',
											'link_before'     => '',
											'link_after'      => '',
											'items_wrap'      => '<ul>%3$s</ul>',
											'depth'           => 4,
											'walker'          => ''));
			?>
			  <?php elseif ($currentlang == "en-GB"): ?>
                    <?php
			echo wp_nav_menu(array('theme_location'  => 'header-menu',
											'menu'            => 'menu-responsive-en',
											'container'       => 'div',
											'container_class' => '',
											'container_id'    => 'nav',
											'menu_class'      => 'menu',
											'menu_id'         => '',
											'echo'            => true,
											'fallback_cb'     => 'wp_page_menu',
											'before'          => '',
											'after'           => '',
											'link_before'     => '',
											'link_after'      => '',
											'items_wrap'      => '<ul>%3$s</ul>',
											'depth'           => 4,
											'walker'          => ''));
			?>

                    <?php endif;?>
		</div>
	</header><!-- #masthead -->
<div id="main" class="site-main clearfix">