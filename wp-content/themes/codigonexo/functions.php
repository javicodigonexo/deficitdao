<?php
function scripts_method() {
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/bootstrap/css/bootstrap.min.css' );
	wp_enqueue_style( 'fontawesome', get_template_directory_uri() . '/font-awesome/css/font-awesome.min.css' );
    wp_enqueue_script(
		'functions-script',
		get_template_directory_uri() . '/js/scripts.js',
		array( 'jquery' )
	);
	wp_enqueue_script(
		'bootstrap',
		get_template_directory_uri() . '/bootstrap/js/bootstrap.min.js',
		array( 'jquery' )
	);
}
add_action( 'wp_enqueue_scripts', 'scripts_method' );
add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
	show_admin_bar(false);
}

function header_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
}
add_action( 'init', 'header_menu' );

// Función para conseguir el parentID
function getParentIdCodigonexo($post) {
    $parent = array_reverse(get_post_ancestors($post->ID));
    $first_parent = get_page($parent[0]);
    return $first_parent->ID;
}
add_action( 'init', 'getParentIdCodigonexo' );

// Función para añadir la clase CSS al menú activo.
function special_nav_class($classes, $item) {
    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'current ';
    }
    return $classes;
}
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

// Función para añadir el campo de footer al administrador
add_action('admin_menu', 'plugins_configuraciones_codigonexo');

function plugins_configuraciones_codigonexo() {
    add_options_page( 'Footer definicíon', 'Footer', 'manage_options', 'footer-definition', 'footer_codigonexo' );
}
function footer_codigonexo() {
    if ( !current_user_can( 'manage_options' ) )  {
        wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
    }

    global $wpdb;
    $table = $wpdb->prefix.'footer';
    global $polylang;
    $lang = $polylang->curlang->slug;

    if(isset($_POST['submit'])){
        if($lang == 'es'){
            $wpdb->update( $table, array('text_footer' => nl2br($_POST['textarea_footer_codigonexo_es'])), array( 'ID' => 1 ), array('%s','%d'), array( '%d' ));
        }else if($lang == 'en'){
            $wpdb->update( $table, array('text_footer' => nl2br($_POST['textarea_footer_codigonexo_en'])), array( 'ID' => 2 ), array('%s','%d'), array( '%d' ));
        }
    }

    $text_footer = $wpdb->get_results( "SELECT text_footer FROM ".$table);
    $text_footer_es = strip_tags($text_footer[0]->text_footer);
    $text_footer_en = strip_tags($text_footer[1]->text_footer);

    echo '<form action="'.$_SERVER['PHP_SELF'].'?page=footer-definition" method="POST">';
    echo '<div class="wrap">';
    echo '<h1>Definir el texto del footer</h1>';
    if(isset($_POST['textarea_footer_codigonexo_es'])){
        echo '<div id="setting-error-settings_updated" class="updated settings-error notice is-dismissible">';
        echo '<p><strong>Ajustes guardados.</strong></p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Descartar este aviso.</span></button></div>';
    }
    echo '<div>';
    echo '<p>'.pll_e('Texto para el footer').'</p>';
    if($lang == 'es'){
        echo '<p><textarea name="textarea_footer_codigonexo_es" style="width: 100%; height: 250px;">'.$text_footer_es.'</textarea></p>';
    }else if($lang == 'en'){
        echo '<p><textarea name="textarea_footer_codigonexo_en" style="width: 100%; height: 250px;">'.$text_footer_en.'</textarea></p>';
    }
    echo '</div>';
    echo '<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Guardar cambios"></p>';
    echo '</form>';
}

function get_user_role($uid) {
    global $wpdb;
    $role = $wpdb->get_var("SELECT meta_value FROM {$wpdb->usermeta} WHERE meta_key = 'wp_capabilities' AND user_id = {$uid}");
      if(!$role) return 'non-user';
    $rarr = unserialize($role);
    $roles = is_array($rarr) ? array_keys($rarr) : array('non-user');
    return $roles[0];
}

// Redirigir a la home si el login es incorrecto
// hook failed login
add_action('wp_login_failed', 'my_front_end_login_fail'); 
function my_front_end_login_fail($username){
    // Get the reffering page, where did the post submission come from?
    $referrer = $_SERVER['HTTP_REFERER'];
 
    // if there's a valid referrer, and it's not the default log-in screen
    if(!empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin')){
        // let's append some information (login=failed) to the URL for the theme to use
        wp_redirect(home_url( '/' ).'?p=178&login=failed'); 
    exit;
    }
}

function my_redirect_login() {
    wp_redirect(home_url( '/' ).'?p=252'); 
    exit;
}
add_action('wp_login', 'my_redirect_login');

// Función para añadir redes sociales
// add_action('admin_menu', 'plugins_redes_sociales_codigonexo');

// function plugins_redes_sociales_codigonexo() {
//     add_options_page( 'Redes Sociales', 'Redes Sociales', 'manage_options', 'redes-sociales', 'redes_sociales_codigonexo' );
// }

// function redes_sociales_codigonexo() {
//     if ( !current_user_can( 'manage_options' ) )  {
//         wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
//     }

//     global $wpdb;
//     $table = $wpdb->prefix.'redes_sociales';
//     if(isset($_POST['submit'])){
//         unset($_POST['submit']);
//         foreach($_POST as $red => $url){
//             $wpdb->update( $table, array('url' => $url), array( 'id' => $red ), array('%s','%d'), array( '%d' ));
//         }
//     }

//     $redes_sociales = $wpdb->get_results( "SELECT * FROM ".$table);
//     $facebook = $redes_sociales[0];
//     $linkedin = $redes_sociales[1];
//     $twitter = $redes_sociales[2];
//     $google = $redes_sociales[3];

//     echo '<form action="'.$_SERVER['PHP_SELF'].'?page=redes-sociales" method="POST">';
//     echo '<div class="wrap">';
//     echo '<h1>Definir el texto del footer</h1>';
//     if(isset($_POST['textarea_footer_codigonexo'])){
//         echo '<div id="setting-error-settings_updated" class="updated settings-error notice is-dismissible">';
//         echo '<p><strong>Ajustes guardados.</strong></p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Descartar este aviso.</span></button></div>';
//     }
//     echo '<table class="form-table">';
//     echo '<tbody>';
//     echo '<tr>';
//     echo '<th scope="row"><label for="mailserver_url">Facebook</label></th>';
//     echo '<td><input type="text" placeholder="Introducir url" style="width: 100%; height: 25px;" name="facebook" value="'.$facebook->url.'" /></td>';
//     echo '</tr>';
//     echo '<tr>';
//     echo '<th scope="row"><label for="mailserver_url">LinkedIn</label></th>';
//     echo '<td><input type="text" placeholder="Introducir url" style="width: 100%; height: 25px;" name="linkedin" value="'.$linkedin->url.'" /></td>';
//     echo '</tr>';
//     echo '<tr>';
//     echo '<th scope="row"><label for="mailserver_url">Twitter</label></th>';
//     echo '<td><input type="text" placeholder="Introducir url" style="width: 100%; height: 25px;" name="twitter" value="'.$twitter->url.'" /></td>';
//     echo '</tr>';
//     echo '<tr>';
//     echo '<th scope="row"><label for="mailserver_url">Google+</label></th>';
//     echo '<td><input type="text" placeholder="Introducir url" style="width: 100%; height: 25px;" name="google_plus" value="'.$google->url.'" /></td>';
//     echo '</tr>';
//     echo '</tbody></table></div>';
//     echo '<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Guardar cambios"></p>';
//     echo '</form>';
// }

function header_idiomas() {
  register_nav_menu('header-idiomas',__( 'Header Idiomas' ));
}
add_action( 'init', 'header_idiomas' );

function footer_menu() {
  register_nav_menu('footer-menu',__( 'Footer Menu' ));
}
add_action( 'init', 'footer_menu' );

function limitaTexto($texto, $limit = 50){
	$recorte = strip_tags($texto);
	$recorte = explode(" ", $recorte);
	$result = "";
	if(count($recorte)> $limit){
		for($e=0;$e<$limit;$e++){
			$result .= $recorte[$e]." ";
		}
		$result .= "...";
		return $result;
	}else{
		return $texto;
	}
}

function pagination($entrada = "post", $range = 4)
{  
	//Poner esto en el OFFSET de get_posts :     (get_option('posts_per_page')*(!isset($paged)  ? 0 : ($paged == 0 ? 0 : $paged-1)));
	// Antes del  get_posts hay que definir: global $paged;
	// Hacer echo de pagination() para mostrar los botones.
     $pages = ((wp_count_posts( $entrada )->publish/get_option('posts_per_page'))< 1 ? 1 : ceil(wp_count_posts( $entrada )->publish/get_option('posts_per_page')));
	 $showitems = ($range * 2)+1;
     global $paged;
     if(empty($paged)) $paged = 1;
     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   
     if(1 != $pages)
     {
         echo "<div class=\"pagination\"><span>".__("Page", $text_domain)." ".$paged." of ".$pages."</span>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";
         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
             }
         }
         if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">Next &rsaquo;</a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
         echo "</div>\n";
     }
}

// Traducciones de Polylang
pll_register_string("title_login_front", "Área de miembros", true);
pll_register_string("text_login_front_login", "Panel", true);
pll_register_string('text_pass_error_not_match', 'Las contraseñas no coinciden', true);
pll_register_string('text_pass_success', 'Contraseña guardada correctamente', true);
pll_register_string('text_pass_new_pass', 'Nueva contraseña', true);
pll_register_string('text_pass_new_pass_confirm', 'Repetir nueva contraseña', true);
pll_register_string('text_pass_error_access_denied', 'Permiso denegado', true);
pll_register_string('text_footer', 'Texto para el footer', true);
pll_register_string('text_login_error', 'Usuario o contraseña incorrectos', true);

if(function_exists('register_sidebar')) {
    register_sidebar(array(
        'name'          => 'Banners',
        'id'            => 'banners',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>',
    ));
}

add_action('wp_logout','go_home');
function go_home(){
  wp_redirect( home_url() );
  exit();
}

// Añadiendo roles nuevos
add_role('socio', __('Socio'), array(
    'read'         => true,  // true allows this capability
    'edit_posts'   => true,
    'delete_posts' => false, // Use false to explicitly deny
));




?>