<?php
/*
Template Name: directorio_socios
*/

get_header(); ?>

<?php if (is_user_logged_in()) { ?>
<div class="col-xs-12 socios">
	<h1><?php echo get_the_title(); ?></h1>
    <h2>Ciencia</h2>
    <?php

        $args = array(
            'blog_id'      => $GLOBALS['blog_id'],
            'role'         => 'socio',
            'meta_key'     => 'wpcf-categoria',
            'meta_value'   => 'ciencia',
            'meta_compare' => '',
            'meta_query'   => array(),
            'date_query'   => array(),        
            'include'      => array(),
            'exclude'      => array(),
            'order'        => 'ASC',
            'offset'       => '',
            'search'       => '',
            'number'       => '',
            'count_total'  => false,
            'fields'       => 'all',
            'who'          => '');

        $science = get_users($args);

        ?>

        <table class="">
            <tr class="head">
                <th>Tro.</th>
                <th>Nombre y apellidos</th>
                <th>Empresa / Institución</th>
                <th>Cargo / Profesión</th>
                <th>E-mail</th>
                <th>Provincia / País</th>
            </tr>
        <?php

        foreach($science as $key => $value){
            $usuario = $value->data;
            $metas = get_user_meta($usuario->ID);
            
            $tratamiento = $metas ["wpcf-tratamiento"][0];
            $nombre = $metas['first_name'][0]." ".$metas['last_name'][0];
            $empresa = $metas["wpcf-empresa-industria"][0];
            $cargo = $metas["wpcf-cargo-profesion"][0];
            $email = $usuario->user_email;
            $provincia = $metas ["wpcf-provincia-pais"][0];
        ?>
            <tr>
                <td><?php echo $tratamiento; ?></td>
                <td><?php echo $nombre; ?></td>
                <td><?php echo $empresa; ?></td>
                <td><?php echo $cargo; ?></td>
                <td><?php echo $email; ?></td>
                <td><?php echo $provincia; ?></td>
            </tr>
        <?php
        }
        ?>
        </table>
		<h2>Innovación e Industria</h2>
<?php

        $args = array(
            'blog_id'      => $GLOBALS['blog_id'],
            'role'         => 'socio',
            'meta_key'     => 'wpcf-categoria',
            'meta_value'   => 'innovacion-industria',
            'meta_compare' => '',
            'meta_query'   => array(),
            'date_query'   => array(),        
            'include'      => array(),
            'exclude'      => array(),
            'order'        => 'ASC',
            'offset'       => '',
            'search'       => '',
            'number'       => '',
            'count_total'  => false,
            'fields'       => 'all',
            'who'          => '');

        $inovacion_y_indistria = get_users($args);

        ?>
		
        <table class="">
            <tr>
                <th>Tro.</th>
                <th>Nombre y apellidos</th>
                <th>Empresa / Institución</th>
                <th>Cargo / Profesión</th>
                <th>E-mail</th>
                <th>Provincia / País</th>
            </tr>
        <?php

        foreach($inovacion_y_indistria as $key => $value){
            $usuario = $value->data;
            $metas = get_user_meta($usuario->ID);
            
            $tratamiento = $metas ["wpcf-tratamiento"][0];
            $nombre = $metas['first_name'][0]." ".$metas['last_name'][0];
            $empresa = $metas["wpcf-empresa-industria"][0];
            $cargo = $metas["wpcf-cargo-profesion"][0];
            $email = $usuario->user_email;
            $provincia = $metas ["wpcf-provincia-pais"][0];
        ?>
            <tr>
                <td><?php echo $tratamiento; ?></td>
                <td><?php echo $nombre; ?></td>
                <td><?php echo $empresa; ?></td>
                <td><?php echo $cargo; ?></td>
                <td><?php echo $email; ?></td>
                <td><?php echo $provincia; ?></td>
            </tr>
        <?php
        }
        ?>
        </table>
		<h2>Sociedad</h2>
<?php        

        $args = array(
            'blog_id'      => $GLOBALS['blog_id'],
            'role'         => 'socio',
            'meta_key'     => 'wpcf-categoria',
            'meta_value'   => 'sociedad',
            'meta_compare' => '',
            'meta_query'   => array(),
            'date_query'   => array(),        
            'include'      => array(),
            'exclude'      => array(),
            'order'        => 'ASC',
            'offset'       => '',
            'search'       => '',
            'number'       => '',
            'count_total'  => false,
            'fields'       => 'all',
            'who'          => '');

        $sociedad = get_users($args);
    ?>
            <table class="">
            <tr>
                <th>Tro.</th>
                <th>Nombre y apellidos</th>
                <th>Empresa / Institución</th>
                <th>Cargo / Profesión</th>
                <th>E-mail</th>
                <th>Provincia / País</th>
            </tr>
        <?php

        foreach($sociedad as $key => $value){
            $usuario = $value->data;
            $metas = get_user_meta($usuario->ID);
            
            $tratamiento = $metas ["wpcf-tratamiento"][0];
            $nombre = $metas['first_name'][0]." ".$metas['last_name'][0];
            $empresa = $metas["wpcf-empresa-industria"][0];
            $cargo = $metas["wpcf-cargo-profesion"][0];
            $email = $usuario->user_email;
            $provincia = $metas ["wpcf-provincia-pais"][0];
        ?>
            <tr>
                <td><?php echo $tratamiento; ?></td>
                <td><?php echo $nombre; ?></td>
                <td><?php echo $empresa; ?></td>
                <td><?php echo $cargo; ?></td>
                <td><?php echo $email; ?></td>
                <td><?php echo $provincia; ?></td>
            </tr>
        <?php
        }
        ?>
        </table>
</div>
<?php wp_reset_postdata();?>

<?php } else { ?>

<div id="sidebar">
    <?php get_sidebar('resonsive'); ?>
</div>
<div id="content">
	<h1><?php echo get_the_title(); ?></h1>
    <?php
        // Start the Loop.
		while ( have_posts() ) : the_post();
			echo the_content();
		endwhile;
	?>
</div>


<?php } ?>

<?php get_footer(); ?>